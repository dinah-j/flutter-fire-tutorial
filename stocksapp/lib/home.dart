import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'firebase.dart';

class HomeView extends StatefulWidget {
  HomeView({Key key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.green,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Home View'),
          // TODO: Add list view with stocks data
          SizedBox(
            height: MediaQuery.of(context).size.height / 2.5,
            child: Placeholder(),
          ),
          IconButton(
            onPressed: () {
              print("Sign Out Pressed");
              context.read<FlutterFireAuthService>().signOut();
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.exit_to_app,
              color: Colors.white,
              size: 35,
            ),
          ),
        ],
      ),
    );
  }
}
